from pathlib import Path

"""
    path_folder_filter permet de définir le chemin
    du repertoire a classé
"""
path_folder_filter = r"C:\Users\NoodleSoft\Downloads\Telegram Desktop"
path = Path(path_folder_filter)

"""
    extension_file est une dictionnaire définissant l'extension a 
    recherché et le repertoire ou mettre ce type de fichier
"""
extension_file = {
    '.torrent': 'Torrents',
    '.jpeg': 'Images',
    '.png': 'Images',
    '.jpg': 'Images',
    '.svg': 'Images',
    '.pdf': 'Document_Pdf_Word_Xls',
    '.docx': 'Document_Pdf_Word_Xls',
    '.xlsx': 'Document_Pdf_Word_Xls',
    '.xltx': 'Document_Pdf_Word_Xls',
    '.sketch': 'UI_Design'
}
# Création du dossier qui contiendra tout les nos dossier filtrer
trie_path = path / "Dossier_filtrer"
trie_path.mkdir(exist_ok=True)

# récupération de tout les fichiers a classé
all_files = [element for element in path.iterdir() if element.is_file()]

for file in all_files:
    folder = extension_file.get(file.suffix) if extension_file.get(
        file.suffix) != None else "Autres"
    outfile_path = trie_path / folder
    outfile_path.mkdir(exist_ok=True)
    file.rename(outfile_path / file.name)

